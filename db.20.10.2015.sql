/*
SQLyog Ultimate v11.21 (64 bit)
MySQL - 5.6.24 : Database - simple_admin_leveling
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `level` */

DROP TABLE IF EXISTS `level`;

CREATE TABLE `level` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(45) DEFAULT NULL,
  `level_last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `level` */

insert  into `level`(`level_id`,`level_name`,`level_last_update`) values (1,'administrator','2015-10-20 10:03:57'),(12,'Tester','2015-10-20 09:57:39');

/*Table structure for table `module` */

DROP TABLE IF EXISTS `module`;

CREATE TABLE `module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_dir` varchar(45) DEFAULT NULL,
  `module_page_sub` varchar(45) DEFAULT NULL,
  `module_page_name` varchar(45) DEFAULT NULL,
  `module_page_default` tinyint(1) DEFAULT '0',
  `module_level_id` int(11) DEFAULT NULL,
  `module_last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`module_id`),
  KEY `module_ibfk_1` (`module_level_id`),
  CONSTRAINT `module_ibfk_1` FOREIGN KEY (`module_level_id`) REFERENCES `level` (`level_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=latin1;

/*Data for the table `module` */

insert  into `module`(`module_id`,`module_dir`,`module_page_sub`,`module_page_name`,`module_page_default`,`module_level_id`,`module_last_update`) values (91,'religion','new_religion','Religion',0,12,'2015-10-20 09:57:39'),(92,'religion','new_religion_proses','Religion',0,12,'2015-10-20 09:57:39'),(93,'religion','religion','Religion',1,12,'2015-10-20 09:57:39'),(114,'level','delete_level','Level',0,1,'2015-10-20 10:03:57'),(115,'level','edit_level','Level',0,1,'2015-10-20 10:03:57'),(116,'level','edit_level_proses','Level',0,1,'2015-10-20 10:03:57'),(117,'level','level','Level',1,1,'2015-10-20 10:03:57'),(118,'level','new_level','Level',0,1,'2015-10-20 10:03:57'),(119,'level','new_level_proses','Level',0,1,'2015-10-20 10:03:57'),(120,'religion','delete_religion','Religion',0,1,'2015-10-20 10:03:57'),(121,'religion','edit_religion','Religion',0,1,'2015-10-20 10:03:57'),(122,'religion','edit_religion_proses','Religion',0,1,'2015-10-20 10:03:57'),(123,'religion','new_religion','Religion',0,1,'2015-10-20 10:03:57'),(124,'religion','new_religion_proses','Religion',0,1,'2015-10-20 10:03:57'),(125,'religion','religion','Religion',1,1,'2015-10-20 10:03:57'),(126,'user','change_password','User',0,1,'2015-10-20 10:03:57'),(127,'user','change_password_proses','User',0,1,'2015-10-20 10:03:57'),(128,'user','delete_user','User',0,1,'2015-10-20 10:03:57'),(129,'user','edit_user','User',0,1,'2015-10-20 10:03:57'),(130,'user','edit_user_proses','User',0,1,'2015-10-20 10:03:57'),(131,'user','new_user','User',0,1,'2015-10-20 10:03:57'),(132,'user','new_user_proses','User',0,1,'2015-10-20 10:03:57'),(133,'user','user','User',1,1,'2015-10-20 10:03:57');

/*Table structure for table `religion` */

DROP TABLE IF EXISTS `religion`;

CREATE TABLE `religion` (
  `religion_id` int(11) NOT NULL AUTO_INCREMENT,
  `religion_name` varchar(45) DEFAULT NULL,
  `religion_last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`religion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `religion` */

insert  into `religion`(`religion_id`,`religion_name`,`religion_last_update`) values (1,'Islam','2015-10-20 09:58:43'),(2,'Christian','2015-10-20 09:59:08'),(3,'Buddha','2015-10-20 09:59:17'),(4,'Kong Hu Cu','2015-10-20 09:59:27'),(5,'Hindu','2015-10-20 09:59:32'),(6,'Protestant','2015-10-20 09:59:58');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `user_level_id` int(11) NOT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_level_id` (`user_level_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`user_level_id`) REFERENCES `level` (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`user_id`,`username`,`password`,`name`,`user_level_id`,`last_login`) values (1,'admin','21232f297a57a5a743894a0e4a801fc3','Administrator',1,'2015-10-20 10:15:28'),(2,'tester','f5d1278e8109edd94e1e4197e04873b9','Tester',12,'2015-10-20 10:00:07'),(3,'tester2','2e9fcf8e3df4d415c96bcf288d5ca4ba','Tester 2',12,'2015-10-20 09:45:39');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

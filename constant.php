<?php

$base_dir =  str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']); // with trailling slash
$base_dir2 = "http" . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "") . "://" . $_SERVER['HTTP_HOST']; // without trailling slash
define('_BASE_DIR', str_replace('\\', '/', dirname(__FILE__)) . '/');
define('_APP_DIR', $base_dir2 . $base_dir);
<!-- javascripts -->
<script src="template/js/jquery-1.8.3.min.js"></script>
<script src="template/js/jquery-ui-1.10.4.min.js"></script>
<script src="template/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="template/js/jquery-ui-1.9.2.custom.min.js"></script>
<!-- bootstrap -->
<script src="template/js/bootstrap.min.js"></script>
<!-- nice scroll -->
<script src="template/js/jquery.scrollTo.min.js"></script>
<script src="template/js/jquery.nicescroll.js" type="text/javascript"></script>
<!-- charts scripts -->
<script src="template/assets/jquery-knob/js/jquery.knob.js"></script>
<script src="template/js/jquery.sparkline.js" type="text/javascript"></script>
<script src="template/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="template/js/owl.carousel.js"></script>
<!-- jQuery full calendar -->
<script src="template/js/fullcalendar.min.js"></script>
<!-- Full Google Calendar - Calendar -->
<script src="template/assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
<!--script for this page only-->
<script src="template/js/calendar-custom.js"></script>
<script src="template/js/jquery.rateit.min.js"></script>
<!-- custom select -->
<script src="template/js/jquery.customSelect.min.js"></script>
<script src="template/assets/chart-master/Chart.js"></script>
<!--custome script for all page-->
<script src="template/js/scripts.js"></script>
<!-- custom script for this page-->
<script src="template/js/sparkline-chart.js"></script>
<script src="template/js/easy-pie-chart.js"></script>
<script src="template/js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="template/js/jquery-jvectormap-world-mill-en.js"></script>
<script src="template/js/xcharts.min.js"></script>
<script src="template/js/jquery.autosize.min.js"></script>
<script src="template/js/jquery.placeholder.min.js"></script>
<script src="template/js/gdp-data.js"></script>
<script src="template/js/morris.min.js"></script>
<script src="template/js/sparklines.js"></script>
<script src="template/js/charts.js"></script>
<script src="template/js/jquery.slimscroll.min.js"></script>
<!-- data tables -->
<script src="template/js/jquery.dataTables.min.js"></script>
<script src="template/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
function Confirm_delete() {
    var t = confirm('Are you sure to delete this item?');
    if (t == true)
        return true;
    else
        return false;
}


$(document).ready(function() {
    $('#dataTables').DataTable({
        responsive: true
    });



    /* tree view */
    $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', 'Collapse this branch');
    /* hide submenu on default */
    $('.tree li.parent_li > ul ').each(function(i, e) {
        var children = $(this).parent('li.parent_li:not(.active)').find(' > ul > li');
        children.hide();
    });
    /* onclick do show or hide submenu */
    $('.tree li.parent_li > span').on('click', function(e) {
        var children = $(this).parent('li.parent_li').find(' > ul > li');
        if (children.is(":visible")) {
            children.hide();
            $(this)
                .attr('title', 'Expand this branch')
                .attr('data-original-title', 'Expand this branch')
                .find(' > i')
                .addClass('fa-chevron-right')
                .removeClass('fa-chevron-left');
        } else {
            children.show();
            $(this)
                .attr('title', 'Collapse this branch')
                .attr('data-original-title', 'Collapse this branch')
                .find(' > i')
                .addClass('fa-chevron-left')
                .removeClass('fa-chevron-right');
        }
        e.stopPropagation();
    });

    /* for tooltip when hover */
    $('[title!=""]').tooltip();


    /*popover demo*/
    $("[data-toggle=popover]").popover()
});
</script>
<script>
//knob
$(function() {
    $(".knob").knob({
        'draw': function() {
            $(this.i).val(this.cv + '%')
        }
    })
});

//carousel
$(document).ready(function() {
    $("#owl-slider").owlCarousel({
        navigation: true,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true

    });
});

//custom select box

$(function() {
    $('select.styled').customSelect();
});

/* ---------- Map ---------- */
$(function() {
    $('#map').vectorMap({
        map: 'world_mill_en',
        series: {
            regions: [{
                values: gdpData,
                scale: ['#000', '#000'],
                normalizeFunction: 'polynomial'
            }]
        },
        backgroundColor: '#eef3f7',
        onLabelShow: function(e, el, code) {
            el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
        }
    });
});

</script>
</body>

</html>

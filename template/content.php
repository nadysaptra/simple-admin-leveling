<!-- container section start -->
<section id="container" class="">
    <header class="header dark-bg">
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"></div>
        </div>
        <!--logo start-->
        <a href="index.html" class="logo">Simple <span class="lite">Admin</span> Management</a>
        <!--logo end-->        
        <div class="top-nav notification-row">
            <!-- notificatoin dropdown start-->
            <ul class="nav pull-right top-menu">                
                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="profile-ava">
                                <img alt="" src="template/img/avatar1_small.jpg">
                            </span>
                        <span class="username"><?php echo $_SESSION['name'];?></span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu logout">
                        <div class="log-arrow-up"></div>
                        <li class="eborder-top">
                            <a href="#"><i class="icon_profile"></i> My Profile</a>
                        </li>                        
                        <li>
                            <a href="login/logout.php"><i class="icon_key_alt"></i> Log Out</a>
                        </li>                        
                    </ul>
                </li>
                <!-- user login dropdown end -->
            </ul>
            <!-- notificatoin dropdown end-->
        </div>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu">
                <li>
                    <a class="" href="index.html">
                        <i class="icon_house_alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <?php include 'menu.php';?>
            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!--overview start-->
            <div class="row">
                <div class="col-lg-12">                    
                    <ol class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="<?php echo _APP_DIR;?>">Home</a></li>
                        <li><i class="fa fa-laptop"></i><?php echo (isset($_GET['c']) ? ucfirst($_GET['c']) : 'Dashboard') ;?></li>
                        <?php echo (isset($_GET['f']) ?  
                            ( ($_GET['c'] !== $_GET['f']) ? '<li><i class="fa fa-laptop"></i>' . ucfirst( str_replace(array('_','-' ,'.'), ' ' , $_GET['f']) ) . '</li>' : '' )
                            : 
                            '');
                        ?>                        
                    </ol>
                </div>
            </div>
            <!-- Today status end -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2><i class="fa fa-flag-o red"></i><strong></strong></h2>                            
                        </div>
                        <div class="panel-body">
                            <?php 
                                CheckModule();
                            ?>
                        </div>
                    </div>
                </div>
                <!--/col-->
            </div>
            <!-- statics end -->
        </section>
    </section>
    <!--main content end-->
</section>
<!-- container section start -->


<?php 

function CheckModule(){
    if(isset($_GET['c'])){
        if(isset($_GET['f'])){
            // check file if exists
            if(file_exists( 'module/' . $_GET['c'] .'/'. $_GET['f'] . '.php')){
                // check if user is assigned to this module and page
                CheckModuleDb($_GET['c'] , $_GET['f'] );                
            }else{
                // not found
                include '404.php';
            }
        }else{
            // if no f then 404
            include '404.php';
        }
    }else{
        echo 'Welcome';
    }
}

function CheckModuleDb($dir , $page){
    $username = $_SESSION['username'];
    $data_module = DB_Fetch("SELECT
                IF(a.`module_id` IS NOT NULL, 1 , 0) AS total
            FROM module a
            WHERE a.`module_dir` = '".$dir."'
            AND a.`module_page_sub` = '".$page."'
            AND a.`module_level_id` IN (
                SELECT aa.`user_level_id`
                FROM `user` aa 
                WHERE aa.`username` = '".$username."'
            )"        
        , true
        );
    if($data_module[0]['total'] > '0')
        include 'module/' . $dir .'/'. $page. '.php';
    else
        include '403.php';
}
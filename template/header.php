<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Simple Admin Leveling - Bootstrap Template</title>

    <!-- Bootstrap CSS -->    
    <link href="template/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="template/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="template/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="template/css/font-awesome.min.css" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="template/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="template/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="template/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="template/css/owl.carousel.css" type="text/css">
	<link href="template/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="template/css/fullcalendar.css">
	<link href="template/css/widgets.css" rel="stylesheet">
    <link href="template/css/style.css" rel="stylesheet">
    <link href="template/css/style-responsive.css" rel="stylesheet" />
	<link href="template/css/xcharts.min.css" rel=" stylesheet">	
	<link href="template/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
    <!-- Datatables -->
    <!-- DataTables CSS -->
    <link href="template/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="template/css/dataTables.responsive.css" rel="stylesheet">

    <link href="template/css/easyTree.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
      <script src="template/js/html5shiv.js"></script>
      <script src="template/js/respond.min.js"></script>
      <script src="template/js/lte-ie7.js"></script>
    <![endif]-->
  </head>

  <body>

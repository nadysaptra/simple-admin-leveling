<a class="btn btn-primary pull-right" href="?c=religion&f=new_religion"><i class="fa fa-plus"></i> Add</a>
<br>
<br>
<div class="dataTable_wrapper">
    <table class="table table-striped table-bordered table-hover" id="dataTables">
        <thead>
            <tr>
                <th>Religion</th>
                <th>Last Update</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $data_religion = DB_Fetch("SELECT * FROM religion" , true);
                foreach ($data_religion as $key => $value) {
                    # code...                
                    echo '<tr class="gradeX">                               
                        <td>'. $value['religion_name'] . '</td>
                        <td>'. date('d/M/Y H:i' , strtotime($value['religion_last_update'])) . '</td>
                        <td>
                            <a href="?c=religion&f=edit_religion&id=' . $value['religion_id'] . '" title="Edit"><i class="fa fa-pencil"></i></a> 
                            <a href="?c=religion&f=delete_religion&id=' .$value['religion_id'] . '" title="Delete" onclick="return Confirm_delete();" ><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>';
            }
            ?>
        </tbody>
    </table>
</div>
<!-- /.table-responsive -->
</div>

<a class="btn btn-primary pull-right" href="?c=user&f=new_user"><i class="fa fa-plus"></i> Add</a>
<br>
<br>
<div class="dataTable_wrapper">
    <table class="table table-striped table-bordered table-hover" id="dataTables">
        <thead>
            <tr>
                <th>Username</th>
                <th>Name</th>
                <th>Level</th>
                <th>Last Login</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $data_user = DB_Fetch("SELECT
                                a.`user_id` AS id,
                                a.`username`,
                                a.`name`,   
                                b.`level_id`,
                                b.`level_name`,
                                a.`last_login`
                            FROM `user` a
                            LEFT JOIN `level` b ON b.`level_id` = a.`user_level_id` 
                            ORDER BY `user_level_id`" , 
                            true
                );
                foreach ($data_user as $key => $value) {
                    # code...                
                    echo '<tr class="gradeX">                               
                        <td>'. $value['username'] . '</td>
                        <td>'. $value['name'] . '</td>
                        <td>'. $value['level_name'] . '</td>
                        <td>'. date("d/M/Y H:i" ,strtotime($value['last_login'])) . '</td>
                        <td>
                            <a href="?c=user&f=edit_user&id=' . $value['id'] . '" title="Edit"><i class="fa fa-pencil"></i></a> 
                            <a href="?c=user&f=delete_user&id=' .$value['id'] . '" title="Delete" onclick="return Confirm_delete();" ><i class="fa fa-trash-o"></i></a>
                            <a href="?c=user&f=change_password&id=' . $value['id'] . '" title="Change Password"><i class="fa fa-recycle"></i></a> 
                        </td>
                    </tr>';
            }
            ?>
        </tbody>
    </table>
</div>
<!-- /.table-responsive -->
</div>


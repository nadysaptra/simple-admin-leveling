<?php
$id = $_GET['id'];
$query = mysql_query("SELECT * FROM `user` WHERE user.`user_id` = $id
") or die(mysql_error());
$data = mysql_fetch_array($query);
?>
    <form action="index.php?c=user&f=edit_user_proses" method="post" role="form">
        <input type="hidden" name="id" value="<?php echo $data['user_id'];?>">
        <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" size="50" class="form-control" value="<?php echo $data['username'];?>">
        </div>
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" size="50" class="form-control" value="<?php echo $data['name'];?>">
        </div>
        <div class="form-group">
            <label>Level</label>
            <select name="level" id="level" class="form-control" onchange="rubah()">
                <option></option>
                <?php
                    $array_level = DB_Fetch(
                        "SELECT
                            a.`level_id` AS `id`,
                            a.`level_name` AS `name`
                        FROM `level` a",
                        true
                    );
                    foreach ($array_level as $key => $value) {
                        # code...
                        if($data['user_level_id'] == $value['id'])
                            $selected = 'selected="selected"';
                        else
                            $selected = '';
                        echo '<option '.$selected.' value="' . $value['id'] . '">'. ucfirst($value['name']) . '</option>';
                    }
                ?>
            </select>
        </div>
        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="Update">
        </div>
    </form>

<form action="index.php?c=user&f=new_user_proses" method="post" role="form">
    <div class="form-group">
        <label>Username</label>
        <input type="text" name="username" size="50" class="form-control">
    </div>
    <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" size="50" class="form-control">
    </div>
    <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama" size="50" class="form-control">
    </div>
    <div class="form-group">
        <label>Level</label>
        <select name="level" id="level" class="form-control" onchange="rubah()">
            <option></option>
            <?php
                $array_level = DB_Fetch(
                    "SELECT
                        a.`level_id` AS `id`,
                        a.`level_name` AS `name`
                    FROM `level` a",
                    true
                    );
                foreach ($array_level as $key => $value) {
                    # code...
                    echo '<option value="' . $value['id'] . '">'. ucfirst($value['name']) . '</option>';
                }
            ?>
        </select>
    </div>    
    <div class="form-group">
        <input class="btn btn-primary" type="submit" value="Tambah">
    </div>
</form>

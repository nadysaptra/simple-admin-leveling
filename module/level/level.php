<a class="btn btn-primary pull-right" href="?c=level&f=new_level"><i class="fa fa-plus"></i> Add</a>
<br>
<br>
<div class="dataTable_wrapper">
    <table class="table table-striped table-bordered table-hover" id="dataTables">
        <thead>
            <tr>
                <th>Level</th>                
                <th>Account Total</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $data_user = DB_Fetch("SELECT
                                a.`level_id` AS `id`,
                                a.`level_name` AS `name`,
                                IFNULL(temp.`total` , 0) as total
                            FROM `level` a
                            LEFT JOIN (
                                SELECT
                                    COUNT(b.`user_id`) AS total,
                                    b.`user_level_id`
                                FROM `user` b
                                GROUP BY b.`user_level_id`
                            ) AS temp ON temp.`user_level_id` = a.`level_id`                            
                        " , 
                            true
                );
                foreach ($data_user as $key => $value) {
                    # code...                
                    echo '<tr class="gradeX">                               
                        <td>'. $value['name'] . '</td>
                        <td>'. $value['total'] . '</td>
                        <td>
                            <a href="?c=level&f=edit_level&id=' . $value['id'] . '" title="Edit"><i class="fa fa-pencil"></i></a> 
                            <a href="?c=level&f=delete_level&id=' .$value['id'] . '" title="Delete" onclick="return Confirm_delete();" ><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>';
            }
            ?>
        </tbody>
    </table>
</div>
<!-- /.table-responsive -->
</div>
<?php 
foreach(glob(_BASE_DIR . 'module/*', GLOB_ONLYDIR) as $dir) {
    $array_ignore_folder = array(
        'New folder',        
        );
    if(in_array(basename($dir) , $array_ignore_folder))
        continue;    
    $dirname[] = basename($dir);
}
?>
<form action="index.php?c=level&f=new_level_proses" method="post" role="form" id="form">
    <div class="form-group">
        <label>Level</label>
        <input type="text" name="level" size="50" class="form-control">
    </div>
    <div class="form-group">
        <label>Authority</label>
        <div class="tree">
            <?php
                echo GetAllDirectory($dirname);
            ?>
        </div>
    </div>    
    <div class="form-group">
        <input class="btn btn-primary" type="button" onclick="Validation()" value="Tambah">
    </div>
</form>


<div class="well">
    <i class="fa fa-exclamation"></i><div class="label label-danger">Choose the menu you want to assign to this level.</div><br/>
    <i class="fa fa-exclamation"></i><div class="label label-danger">Choose one of the list for main page for menu in the "Sidebar Menu"</div>
</div>

<style type="text/css">
    .well-sm{
        padding: 0px 9px !important;
    }
    ul {
        margin-bottom: 1px !important;
    }
</style>

<script type="text/javascript">
    function Validation(){
        /* check the checked checkbox */
        var all_checked = $('input[type="checkbox"]:checked');
        var arr_dir = [];
        all_checked.each( function(index){        
            if(typeof $(this).attr('class') !== 'undefined' )
                arr_dir.push($(this).attr('class'));            
        })
        /* get array unique */
        arr_dir = $.unique(arr_dir)
        /* check whether the select for default menu was not empty */
        var result = true;
        $.each( arr_dir, function(i,v){
            var select_default = $('.select_'+ v).val();
            if( select_default == '' ){
                alert( 'Please select on option of dropdown "'+ v + '"' );                
                result &= false;
                return false;
            }
        })
        if(result)
            $('form').submit()
    }

    function CheckAll(value){
        var check = $("#form").find('.'+ value);
        if($('#check_all_' + value).is(':checked')){
           check.each(function(){
              $(this).prop('checked', true); 
           });
        }else{
           check.each(function(){
              $(this).prop('checked' , false); 
           });
        }
    }

    

    function CheckBoxClick(value){        
        var data = $("#form").find('.'+value);
        var chek = $("#form").find('.'+value+':checked');
        if(data.length == chek.length){
            $("#check_all_" + value).prop('checked',true);
        }else{
            $("#check_all_" + value).prop('checked',false);
        }
    }
      
     
</script>


<?php 

function GetAllDirectory($dirname){
    $html = '';
    foreach ($dirname as $key => $value) {
        # code...
        $html .= '<ul>';
        $html .= '<li><span><i class="fa fa-chevron-right"></i></span>';
        // check into subfolder
            $html .= GetAllFileInDirectory($value);
        $html .= '</li>';

        $html .= '</ul>';
    }
    return $html;
}

function GetAllFileInDirectory($value){
    $html = '';
    $check_subfolder = glob(_BASE_DIR . 'module/'. $value . '/*');    
    if(!empty($check_subfolder)){
        $html .= '<label class="well-sm"><input type="checkbox" id="check_all_'.$value.'" onclick="CheckAll(\''.$value.'\')" name="tree['.$value.']" name="'.$value.'"> ' . ucfirst($value) . '</label>';
        $html .= '<div class="label label-info ">Default 
        <select name="tree['.$value.'][default]" class="select_'.$value.'">
            <option value="">-- Choose One --</option>';

        foreach(glob(_BASE_DIR . 'module/'. $value . '/*') as $dir)
            $html .= '<option value="'.pathinfo($dir, PATHINFO_FILENAME).'">'.basename($dir).'</option>';

        $html .= '</select>
        </div>';
        $html .= '<ul>';
        foreach(glob(_BASE_DIR . 'module/'. $value . '/*') as $dir) {
            $subdirname = basename($dir);
            $html .= '<li><span><i class="fa fa-chevron-right"></i> <input type="checkbox" class="'.$value.'" onclick="CheckBoxClick(\''.$value.'\')" name="tree['.$value.'][module][]" value="'.pathinfo($dir, PATHINFO_FILENAME).'"> ' . $subdirname . '</span> </li>';
        }
        $html .= '</ul>';
    }else{
        $html .= '<label class="well-sm">' . ucfirst($value) . '</label>';
    }
    return $html;
}
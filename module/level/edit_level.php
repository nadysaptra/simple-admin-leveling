<?php 
// get data by id
$id = $_GET['id'];
$data_level = DB_Fetch(
    "SELECT * FROM level WHERE level_id = " . $id,
    true
    );
// it is to use to ignore folder by name
foreach(glob(_BASE_DIR . 'module/*', GLOB_ONLYDIR) as $dir) {
    $array_ignore_folder = array(
        'New folder',        
        'Untitled Folder',        
        );
    if(in_array(basename($dir) , $array_ignore_folder))
        continue;    
    $dirname[] = basename($dir);
}
?>
<form action="index.php?c=level&f=edit_level_proses" method="post" role="form" id="form">
    <input type="hidden" value="<?php echo $id;?>" name="id">
    <div class="form-group">
        <label>Level</label>
        <input type="text" name="level" size="50" class="form-control" value="<?php echo $data_level[0]['level_name'];?>">
    </div>
    <div class="form-group">
        <label>Authority</label>
        <div class="tree">
            <?php
                echo GetAllDirectory($dirname);
            ?>
        </div>
    </div>    
    <div class="form-group">
        <input class="btn btn-primary" type="button" onclick="Validation()" value="Update">
    </div>
</form>


<div class="well">
    <i class="fa fa-exclamation"></i><div class="label label-danger">Choose the menu you want to assign to this level.</div><br/>
    <i class="fa fa-exclamation"></i><div class="label label-danger">Choose one of the list for main page for menu in the "Sidebar Menu"</div>
</div>

<style type="text/css">
    .well-sm{
        padding: 0px 9px !important;
    }
    ul {
        margin-bottom: 1px !important;
    }
</style>

<script type="text/javascript">
    function Validation(){
        /* check the checked checkbox */
        var all_checked = $('input[type="checkbox"]:checked');
        var arr_dir = [];
        all_checked.each( function(index){        
            if(typeof $(this).attr('class') !== 'undefined' )
                arr_dir.push($(this).attr('class'));            
        })
        /* get array unique */
        arr_dir = $.unique(arr_dir)
        /* check whether the select for default menu was not empty */
        var result = true;
        $.each( arr_dir, function(i,v){
            var select_default = $('.select_'+ v).val();
            if( select_default == '' ){
                alert( 'Please select on option of dropdown "'+ v + '"' );                
                result &= false;
                return false;
            }
        })
        if(result)
            $('form').submit()
    }

    function CheckAll(value){
        var check = $("#form").find('.'+ value);
        if($('#check_all_' + value).is(':checked')){
           check.each(function(){
              $(this).prop('checked', true); 
           });
        }else{
           check.each(function(){
              $(this).prop('checked' , false); 
           });
        }
    }

    

    function CheckBoxClick(value){        
        var data = $("#form").find('.'+value);
        var chek = $("#form").find('.'+value+':checked');
        if(data.length == chek.length){
            $("#check_all_" + value).prop('checked',true);
        }else{
            $("#check_all_" + value).prop('checked',false);
        }
    }
      
     
</script>


<?php

function GetAllDirectory($dirname){
    $html = '';
    $id = $_GET['id'];
    //~ get all data module
    $getDataModule = DB_Fetch("SELECT *
            FROM module a
            WHERE a.`module_level_id` = ". $id , true);
    //~ create array by directory 
    $dataModule = array();
    foreach ($getDataModule as $key => $value) {
        # code...
        $dataModule[$value['module_dir']]['assigned'][] = $value['module_page_sub'];
        if($value['module_page_default'] == '1')
            $dataModule[$value['module_dir']]['default'][] = $value['module_page_sub'];
    }
    foreach ($dirname as $key => $value) {
        # code...
        $html .= '<ul>';
        if(!empty($dataModule[$value]['assigned'])){
            $active = 'active';
            $chevron = 'left';
        }else{
            $active = '';
            $chevron = 'right';
        }
        $html .= '<li class="'.$active.'"><span><i class="fa fa-chevron-'.$chevron.'"></i></span>';
        // check into subfolder
            $html .= GetAllFileInDirectory($value , $dataModule[$value]);
        $html .= '</li>';

        $html .= '</ul>';
    }
    return $html;
}

function GetAllFileInDirectory($value , $dataModule){    
    $html = '';
    $dataModule = !empty($dataModule) ? $dataModule : array();    
    $check_subfolder = glob(_BASE_DIR . 'module/'. $value . '/*');        
    if(!empty($check_subfolder)){
        $count_selected_files = sizeof($dataModule['assigned']);
        $count_all_files = sizeof($check_subfolder);        
        $checked_all = ($count_selected_files == $count_all_files) ? 'checked="checked"' : '';
        $html .= '<label class="well-sm">
            <input '.$checked_all.' type="checkbox" id="check_all_'.$value.'" onclick="CheckAll(\''.$value.'\')" name="tree['.$value.']" name="'.$value.'"> ' 
            . ucfirst($value) 
        . '</label>';
        $html .= '<div class="label label-info ">Default 
        <select name="tree['.$value.'][default]" class="select_'.$value.'">
            <option value="">-- Choose One --</option>';

        foreach(glob(_BASE_DIR . 'module/'. $value . '/*') as $dir){
            if(pathinfo($dir, PATHINFO_FILENAME) == $dataModule['default'][0])
                $selected = 'selected="selected"';
            else
                $selected = '';
            $html .= '<option '.$selected.' value="'.pathinfo($dir, PATHINFO_FILENAME).'">'.basename($dir).'</option>';
        }

        $html .= '</select>
        </div>';
        $html .= '<ul>';        
        foreach(glob(_BASE_DIR . 'module/'. $value . '/*') as $dir) {
            $subdirname = basename($dir);
            if(in_array(pathinfo($dir, PATHINFO_FILENAME), $dataModule['assigned']))
                $checked = 'checked="checked"';
            else
                $checked = '';
            $html .= '<li><span><i class="fa fa-chevron-right"></i> 
                <input type="checkbox" '.$checked.' class="'.$value.'" onclick="CheckBoxClick(\''.$value.'\')" name="tree['.$value.'][module][]" value="'.pathinfo($dir, PATHINFO_FILENAME).'"> ' . 
                $subdirname . '</span> 
            </li>';
        }
        $html .= '</ul>';
    }else{
        $html .= '<label class="well-sm">' . ucfirst($value) . '</label>';
    }
    return $html;
}